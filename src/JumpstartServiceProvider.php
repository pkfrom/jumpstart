<?php

namespace Fromz\Jumpstart;

use Fromz\Jumpstart\Commands\Jumpstart;
use Illuminate\Support\ServiceProvider;

class JumpstartServiceProvider extends ServiceProvider
{
    protected $commands = [
        Jumpstart::class,
    ];

    /**
     * Register the service provider.
     */
    public function register()
    {
        $this->commands($this->commands);
    }

    /**
     * Perform post-registration booting of services.
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/views', 'jumpstart');
    }
}
